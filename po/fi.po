# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Ammuu5, 2017-2018
# Jiri Grönroos <jiri.gronroos@iki.fi>, 2018-2020
# Pasi Lallinaho <pasi@shimmerproject.org>, 2015
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-04-09 00:32+0200\n"
"PO-Revision-Date: 2020-06-10 20:32+0000\n"
"Last-Translator: Jiri Grönroos <jiri.gronroos@iki.fi>\n"
"Language-Team: Finnish (http://www.transifex.com/xfce/xfce-panel-plugins/language/fi/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fi\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../data/appdata/xfce4-clipman.appdata.xml.in.h:1
msgid ""
"Clipboard Manager provided as a panel plugin for Xfce and as a standalone "
"application running in the notification area. It keeps a history of text and"
" images of content copied to the clipboard. It also has a feature to execute"
" actions on specific text selection by matching them against regexes."
msgstr ""

#: ../panel-plugin/xfce4-clipman.desktop.in.h:1
msgid "Clipboard Manager"
msgstr "Leikepöydän hallinta"

#: ../panel-plugin/xfce4-clipman.desktop.in.h:2
msgid "Clipboard Manager Autostart File"
msgstr ""

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:1
msgid "Image"
msgstr "Kuva"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:2
msgid "Edit with Gimp"
msgstr "Muokkaa GIMP:ssä"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:3
msgid "View with Ristretto"
msgstr "Näytä Ristrettolla"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:4
msgid "Bugz"
msgstr "Vikailmoitukset"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:5
msgid "Xfce Bug"
msgstr "Xfce:n vikailmoitus"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:6
msgid "GNOME Bug"
msgstr "GNOME-vikailmoitus"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:7
msgid "Long URL"
msgstr "Pitkä URL"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:8
msgid "Shrink the URL"
msgstr "Luo lyhyt URL"

#: ../panel-plugin/xfce4-clipman-plugin.desktop.in.h:1
#: ../panel-plugin/xfce4-clipman-plugin-autostart.desktop.in.h:1
#: ../panel-plugin/main-panel-plugin.c:77
#: ../panel-plugin/main-status-icon.c:81 ../panel-plugin/plugin.c:95
#: ../panel-plugin/plugin.c:333
msgid "Clipman"
msgstr "Clipman"

#: ../panel-plugin/xfce4-clipman-plugin.desktop.in.h:2
#: ../panel-plugin/xfce4-clipman-plugin-autostart.desktop.in.h:2
msgid "Clipboard manager"
msgstr "Leikepöydän hallinta"

#: ../panel-plugin/xfce4-clipman-settings.c:97
msgid "None"
msgstr "Ei mitään"

#. TRANSLATORS: Keyboard shortcut
#: ../panel-plugin/xfce4-clipman-settings.c:99
msgid "Ctrl+V"
msgstr "Ctrl+V"

#. TRANSLATORS: Keyboard shortcut
#: ../panel-plugin/xfce4-clipman-settings.c:101
msgid "Shift+Insert"
msgstr "Vaihto+Insert"

#: ../panel-plugin/xfce4-clipman-settings.c:535
msgid "<b>Reset actions</b>"
msgstr "<b>Nollaa toiminnot</b>"

#: ../panel-plugin/xfce4-clipman-settings.c:537
msgid ""
"Are you sure you want to reset the actions to the system default values?"
msgstr "Haluatko varmasti palauttaa toiminnot järjestelmän oletusarvoihin?"

#: ../panel-plugin/settings-dialog.ui.h:1
msgid "Clipman Settings"
msgstr "Clipmanin asetukset"

#: ../panel-plugin/settings-dialog.ui.h:2
#: ../panel-plugin/xfce4-clipman-history.c:441
#: ../panel-plugin/xfce4-clipman-history.c:443
msgid "_Help"
msgstr "_Ohje"

#: ../panel-plugin/settings-dialog.ui.h:3
msgid "_Close"
msgstr "_Sulje"

#: ../panel-plugin/settings-dialog.ui.h:4
msgid "Sync mouse _selections"
msgstr "_Synkronoi hiirivalinnat"

#: ../panel-plugin/settings-dialog.ui.h:5
msgid ""
"If checked, the selections will be synced with the default clipboard in a "
"way that you can paste what you select"
msgstr "Valinnat päivitetään oletusleikepöydälle siten, että voit liittää, mitä valitset."

#: ../panel-plugin/settings-dialog.ui.h:6
msgid "_QR-Code support"
msgstr "_QR-koodien tuki"

#: ../panel-plugin/settings-dialog.ui.h:7
msgid ""
"If checked, the menu shows a QR-Code of the currently selected clipboard "
"entry"
msgstr "Valikko näyttää valitun leikepöydän merkinnän QR-koodin"

#: ../panel-plugin/settings-dialog.ui.h:8
msgid "Automatically paste a selected item from the history"
msgstr "Liitä automaattisesti valittu historian kohde"

#: ../panel-plugin/settings-dialog.ui.h:9
msgid "_Paste instantly:"
msgstr "_Liitä heti:"

#: ../panel-plugin/settings-dialog.ui.h:10
msgid "<b>General</b>"
msgstr "<b>Yleiset</b>"

#: ../panel-plugin/settings-dialog.ui.h:11
msgid "P_osition menu at mouse pointer"
msgstr "Avaa valikko _hiiren kohdalla"

#: ../panel-plugin/settings-dialog.ui.h:12
msgid ""
"Popup the menu at the mouse pointer position, only for the xfce4-popup-"
"clipman command"
msgstr "Näytä valikko hiiren osoittimen kohdalla, vain käskyllä xfce4-popup-clipman"

#: ../panel-plugin/settings-dialog.ui.h:13
msgid "Maximum items:"
msgstr "Kohteita enintään:"

#: ../panel-plugin/settings-dialog.ui.h:14
msgid "<b>Menu</b>"
msgstr "<b>Valikko</b>"

#: ../panel-plugin/settings-dialog.ui.h:15
msgid "_Behavior"
msgstr "_Toiminta"

#: ../panel-plugin/settings-dialog.ui.h:16
msgid "<b>_Enable automatic actions</b>"
msgstr "<b>_Käytä automaattisia toimintoja</b>"

#: ../panel-plugin/settings-dialog.ui.h:17
msgid ""
"If checked, the clipboard texts will be matched against regular expressions and a menu will display possible actions automatically.\n"
"Otherwise the menu will only appear when calling \"xfce4-popup-clipman-actions\" (and a match was found)"
msgstr ""

#: ../panel-plugin/settings-dialog.ui.h:19
msgid "_Show actions by holding Control"
msgstr "_Näytä toiminnot pitämällä pohjassa Control"

#: ../panel-plugin/settings-dialog.ui.h:20
msgid ""
"When the selection is done (mouse or keyboard) and the Control key is still "
"pressed down, the popup menu for matched actions will be shown"
msgstr "Kun valinta on tehty (hiirellä tai näppäimillä) ja Ctrl on yhä painettuna, vastaavien toimintojen ponnahdusvalikko ohitetaan"

#: ../panel-plugin/settings-dialog.ui.h:21
msgid "S_kip actions by holding Control"
msgstr "Ohita toiminnot pitämällä pohjassa _Control"

#: ../panel-plugin/settings-dialog.ui.h:22
msgid ""
"When the selection is done (mouse or keyboard) and the Control key is still "
"pressed down, the popup menu for matched actions will be skipped"
msgstr "Kun valinta on tehty (hiirellä tai näppäimillä) ja Ctrl on yhä painettuna, vastaavien toimintojen ponnahdusvalikko ohitetaan"

#: ../panel-plugin/settings-dialog.ui.h:23
msgid "Add action"
msgstr "Lisää toiminto"

#: ../panel-plugin/settings-dialog.ui.h:24
msgid "Edit action"
msgstr "Muokkaa toimintoa"

#: ../panel-plugin/settings-dialog.ui.h:25
msgid "Delete action"
msgstr "Poista toiminto"

#: ../panel-plugin/settings-dialog.ui.h:26
msgid "Reset all actions to the system default values"
msgstr "Palauta kaikki toiminnot järjestelmän oletusarvoihin"

#: ../panel-plugin/settings-dialog.ui.h:27
msgid "_Actions"
msgstr "_Toiminnot"

#: ../panel-plugin/settings-dialog.ui.h:28
msgid "<b>Remember history</b>"
msgstr "<b>Muista historia</b>"

#: ../panel-plugin/settings-dialog.ui.h:29
msgid "Remember last copied _image"
msgstr "_Muista viimeksi kopioitu kuva"

#: ../panel-plugin/settings-dialog.ui.h:30
msgid ""
"If checked, this option allows one to store one image inside the history"
msgstr ""

#: ../panel-plugin/settings-dialog.ui.h:31
msgid "_Reorder history items"
msgstr "_Järjestä historian kohteita"

#: ../panel-plugin/settings-dialog.ui.h:32
msgid ""
"Push last copied text to the top of the history, useful to reorder old items"
msgstr "Siirrä viimeksi kopioitu teksti historian ylimmäksi; helpottaa vanhojen kohteiden järjestämistä"

#: ../panel-plugin/settings-dialog.ui.h:33
msgid "Re_verse history order"
msgstr "Käännä historian järjestys"

#: ../panel-plugin/settings-dialog.ui.h:34
msgid "Reverse order of the history shown in the menu"
msgstr "Käännä valikossa näytettävän historian järjestys"

#: ../panel-plugin/settings-dialog.ui.h:35
msgid "Ignore mouse s_elections"
msgstr ""

#: ../panel-plugin/settings-dialog.ui.h:36
msgid ""
"If checked, the selections won't affect the history except the manual copies"
msgstr "Valinnat vaikuttavat historiaan ainoastaan käsin kopioitaessa"

#: ../panel-plugin/settings-dialog.ui.h:37
msgid "Size of the _history:"
msgstr "Historian k_oko:"

#: ../panel-plugin/settings-dialog.ui.h:38
msgid "5"
msgstr "5"

#: ../panel-plugin/settings-dialog.ui.h:39
msgid "H_istory"
msgstr "_Historia"

#: ../panel-plugin/settings-dialog.ui.h:40
msgid "Edit Action"
msgstr "Muokkaa toimintoa"

#: ../panel-plugin/settings-dialog.ui.h:41
msgid "Name:"
msgstr "Nimi:"

#: ../panel-plugin/settings-dialog.ui.h:42
msgid "Pattern:"
msgstr "Lauseke:"

#: ../panel-plugin/settings-dialog.ui.h:43
msgid ""
"You can use the substitution parameters \"\\1\", \"\\2\" and so on in the "
"commands. The parameter \"\\0\" represents the complete text. The pattern is"
" always anchored within the special characters ^$"
msgstr ""

#: ../panel-plugin/settings-dialog.ui.h:44
msgid "Activate only on manual copy"
msgstr "Ainoastaan käsin kopioitaessa"

#: ../panel-plugin/settings-dialog.ui.h:45
msgid ""
"By default the action is triggered by a selection, check this option to "
"trigger the action only when you make a manual copy"
msgstr ""

#: ../panel-plugin/settings-dialog.ui.h:46
msgid "<b>Action</b>"
msgstr "<b>Toiminto</b>"

#: ../panel-plugin/settings-dialog.ui.h:47
msgid "Command:"
msgstr "Komento:"

#: ../panel-plugin/settings-dialog.ui.h:48
msgid "<b>Commands</b>"
msgstr "<b>Komennot</b>"

#: ../panel-plugin/settings-dialog.ui.h:49
msgid "Type here your custom text, for example a URL, a filename, etc."
msgstr ""

#: ../panel-plugin/settings-dialog.ui.h:50
msgid "Regular expression"
msgstr "Säännöllinen lauseke"

#: ../panel-plugin/main-panel-plugin.c:111
#: ../panel-plugin/main-status-icon.c:159
msgid "_Disable"
msgstr "_Poista käytöstä"

#: ../panel-plugin/actions.c:368
#, c-format
msgid ""
"Unable to execute the command \"%s\"\n"
"\n"
"%s"
msgstr "Komennon \"%s\" suoritus ei onnistu\n\n%s"

#: ../panel-plugin/menu.c:255
msgid "Are you sure you want to clear the history?"
msgstr "Haluatko varmasti tyhjentää historian?"

#: ../panel-plugin/menu.c:260
msgid "Don't ask again"
msgstr "Älä kysy uudelleen"

#: ../panel-plugin/menu.c:306
msgid "Unable to open the clipman history dialog"
msgstr ""

#: ../panel-plugin/menu.c:455
msgid "Could not generate QR-Code."
msgstr "QR-koodin luominen epäonnistui."

#. Insert empty menu item
#: ../panel-plugin/menu.c:469 ../panel-plugin/xfce4-clipman-history.c:334
msgid "Clipboard is empty"
msgstr "Leikepöytä on tyhjä"

#: ../panel-plugin/menu.c:602
msgid "_Show full history..."
msgstr "_Näytä koko historia..."

#: ../panel-plugin/menu.c:608
msgid "_Clear history"
msgstr "_Tyhjennä historia"

#: ../panel-plugin/menu.c:615
msgid "_Clipman settings..."
msgstr "_Clipmanin asetukset..."

#: ../panel-plugin/plugin.c:321
msgid "Contributors:"
msgstr "Avustavat kehittäjät:"

#: ../panel-plugin/plugin.c:335
msgid "Clipboard Manager for Xfce"
msgstr "Leikepöytien hallinta Xfce:lle"

#: ../panel-plugin/plugin.c:343
msgid "translator-credits"
msgstr "Lassi Kojo <lassi.kojo@sivutpystyyn.net>\nJari Rahkonen <jari.rahkonen@pp1.inet.fi>"

#: ../panel-plugin/plugin.c:357
msgid "Unable to open the settings dialog"
msgstr "Asetusikkunaa ei voi avata"

#: ../panel-plugin/common.c:29
msgid ""
"Could not start the Clipboard Manager Daemon because it is already running."
msgstr ""

#: ../panel-plugin/common.c:30
msgid "The Xfce Clipboard Manager is already running."
msgstr ""

#: ../panel-plugin/common.c:37
msgid "You can launch it with 'xfce4-clipman'."
msgstr ""

#: ../panel-plugin/common.c:39
msgid "The Clipboard Manager Daemon is not running."
msgstr ""

#: ../panel-plugin/xfce4-clipman-history.c:256
msgid "Enter search phrase here"
msgstr "Kirjoita hakuehto tähän"

#: ../panel-plugin/xfce4-clipman-history.c:414
#, c-format
msgid "_Paste"
msgstr "_Liitä"

#: ../panel-plugin/xfce4-clipman-history.c:419
#, c-format
msgid "_Copy"
msgstr "_Kopioi"

#: ../panel-plugin/xfce4-clipman-history.c:435
msgid "Clipman History"
msgstr "Clipmanin historia"

#: ../panel-plugin/xfce4-clipman-history.c:449
#: ../panel-plugin/xfce4-clipman-history.c:451
msgid "_Settings"
msgstr "_Asetukset"

#: ../panel-plugin/xfce4-clipman-settings.desktop.in.h:1
msgid "Clipboard Manager Settings"
msgstr "Leikepöydän hallinnan asetukset"

#: ../panel-plugin/xfce4-clipman-settings.desktop.in.h:2
msgid "Customize your clipboard"
msgstr "Mukauta leikepöytääsi"
